#!/bin/bash

#Script to convert Maryam monthly ensemble into annual differences

data_in_path="/home/ucakmil/lk_non_par_ensemble"
data_out_path="/data/aod/Ilyasetal2017_annual"

COUNTER=1
while [  $COUNTER -le 100 ]
do
  cd $data_in_path/hd_ens$COUNTER
  for fil in ensemble*.nc
  do 
    ncra -O --mro -d time,,,12,12 $fil /tmp/ann.$fil
  done  
  ncecat -O -u ens_mem /tmp/ann.ensemble*.nc $data_out_path/ann_hd_ens$COUNTER.nc
  let COUNTER=COUNTER+1 
done
