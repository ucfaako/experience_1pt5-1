rm(list=ls())

#example-1
library(sROC)
set.seed(100)
n <- 200
x <- c(rnorm(n/2, mean=-2, sd=1), rnorm(n/2, mean=3, sd=0.8))
x.CDF <- kCDF(x)
x.CDF
CI.CDF(x.CDF)
X11()
plot(x.CDF, alpha=0.05, main="Kernel estimate of distribution function")
curve(pnorm(x, mean=-2, sd=1)/2 + pnorm(x, mean=3, sd=0.8)/2, from =-6, to=6, add=TRUE, lty=2, col="blue")

#example-2
# calculate and plot the simultaneous confidence bands from DKW inequality
# first we repeat the data generation
n			<-	100  			
x			<-  rexp(n,1) #observations		
x			<-	sort(x) 		
FF			<-	ecdf(x)			
# then we calculate the confidence band
alpha		<-	0.05
eps 		<-	sqrt(log(2/alpha)/(2*n))
xx			<-	seq(min(x)-1,max(x)+1,length.out=1000)
ll			<-	pmax(FF(xx)-eps,0) 		# pmin and pmax do element wise min/max;  min and max would find the min/max of the entire vector
uu 			<-	pmin(FF(xx)+eps,1)
# then we plot everything
X11()
plot(FF, cex=0.25)
lines(xx, ll, col="blue") 		# lines (also points) doesn't need the add=TRUE command
lines(xx, uu, col="blue")
# you can add a plot of the true cdf in this case (since we know it)
Ftrue		<-	function(x) {pexp(x,1)}			# define the true cdf
Ftrue(1:5)				
plot(Ftrue, add=TRUE, col="red", xlim=c(-3,10))




rm(list=ls())
f <- ecdf(rnorm(100)) 
x <- rnorm(10) 
y <- f(x) 

#If you want to get the x corresponding to given y, use linear interpolation. 

inv_ecdf <- function(f){ 
        x <- environment(f)$x 
        y <- environment(f)$y 
        approxfun(y, x) 
} 

g <- inv_ecdf(f) 
g(0.5) 
