rm(list=ls())
library(ncdf4)

##Length of the time series is different 75 to 547

##Model-names
A1<- c("ACCESS1-0","ACCESS1-3","bcc-csm1-1","bcc-csm1-1-m","BNU-ESM","CanCM4","CanESM2","CCSM4","CESM1-BGC","CESM1-CAM5",
       "CMCC-CESM","CMCC-CM","CMCC-CMS","CNRM-CM5","CSIRO-Mk3-6-0","FGOALS-g2", "FIO-ESM","GFDL-CM3","GFDL-ESM2G","GFDL-ESM2M",
       "GISS-E2-H","GISS-E2-H-CC","GISS-E2-R","HadCM3","HadGEM2-AO","HadGEM2-CC","HadGEM2-ES","inmcm4","IPSL-CM5A-LR","IPSL-CM5A-MR",
       "IPSL-CM5B-LR","MIROC4h","MIROC5","MIROC-ESM","MIROC-ESM-CHEM","MPI-ESM-LR","MPI-ESM-MR","MRI-CGCM3","MRI-ESM1","NorESM1-M",
       "NorESM1-ME")

##files in 1st model
A1_1<- c("ann_gm.ts_Amon_ACCESS1-0.rcp45","ann_gm.ts_Amon_ACCESS1-0.rcp85")
A1_2<- c("ann_gm.ts_Amon_ACCESS1-3.rcp45","ann_gm.ts_Amon_ACCESS1-3.rcp85")
A1_3<- c("ann_gm.ts_Amon_bcc-csm1-1.rcp26","ann_gm.ts_Amon_bcc-csm1-1.rcp45","ann_gm.ts_Amon_bcc-csm1-1.rcp60","ann_gm.ts_Amon_bcc-csm1-1.rcp85")
A1_4<- c("ann_gm.ts_Amon_bcc-csm1-1-m.rcp26","ann_gm.ts_Amon_bcc-csm1-1-m.rcp45","ann_gm.ts_Amon_bcc-csm1-1-m.rcp60","ann_gm.ts_Amon_bcc-csm1-1-m.rcp85")
A1_5<- c("ann_gm.ts_Amon_BNU-ESM.rcp26","ann_gm.ts_Amon_BNU-ESM.rcp45","ann_gm.ts_Amon_BNU-ESM.rcp85")
A1_6<- c("ann_gm.ts_Amon_CanCM4.rcp45")
A1_7<- c("ann_gm.ts_Amon_CanESM2.rcp26","ann_gm.ts_Amon_CanESM2.rcp45")
A1_8<- c("ann_gm.ts_Amon_CCSM4.rcp26","ann_gm.ts_Amon_CCSM4.rcp45","ann_gm.ts_Amon_CCSM4.rcp60","ann_gm.ts_Amon_CCSM4.rcp85")
A1_9<- c("ann_gm.ts_Amon_CESM1-BGC.rcp45","ann_gm.ts_Amon_CESM1-BGC.rcp85")
A1_10<- c("ann_gm.ts_Amon_CESM1-CAM5.rcp26","ann_gm.ts_Amon_CESM1-CAM5.rcp45","ann_gm.ts_Amon_CESM1-CAM5.rcp60","ann_gm.ts_Amon_CESM1-CAM5.rcp85")
A1_11<- c("ann_gm.ts_Amon_CMCC-CESM.rcp85")
A1_12<- c("ann_gm.ts_Amon_CMCC-CM.rcp45","ann_gm.ts_Amon_CMCC-CM.rcp85")
A1_13<- c("ann_gm.ts_Amon_CMCC-CMS.rcp45","ann_gm.ts_Amon_CMCC-CMS.rcp85")
A1_14<- c("ann_gm.ts_Amon_CNRM-CM5.rcp26","ann_gm.ts_Amon_CNRM-CM5.rcp45","ann_gm.ts_Amon_CNRM-CM5.rcp85")
A1_15<- c("ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp26","ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp45","ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp60","ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp85")
A1_16<- c("ann_gm.ts_Amon_FGOALS-g2.rcp26","ann_gm.ts_Amon_FGOALS-g2.rcp45","ann_gm.ts_Amon_FGOALS-g2.rcp85")
A1_17<- c("ann_gm.ts_Amon_FIO-ESM.rcp26","ann_gm.ts_Amon_FIO-ESM.rcp45","ann_gm.ts_Amon_FIO-ESM.rcp60","ann_gm.ts_Amon_FIO-ESM.rcp85")
A1_18<- c("ann_gm.ts_Amon_GFDL-CM3.rcp26","ann_gm.ts_Amon_GFDL-CM3.rcp45","ann_gm.ts_Amon_GFDL-CM3.rcp85")
A1_19<- c("ann_gm.ts_Amon_GFDL-ESM2G.rcp26","ann_gm.ts_Amon_GFDL-ESM2G.rcp45","ann_gm.ts_Amon_GFDL-ESM2G.rcp60","ann_gm.ts_Amon_GFDL-ESM2G.rcp85")
A1_20<- c("ann_gm.ts_Amon_GFDL-ESM2M.rcp26","ann_gm.ts_Amon_GFDL-ESM2M.rcp45","ann_gm.ts_Amon_GFDL-ESM2M.rcp60","ann_gm.ts_Amon_GFDL-ESM2M.rcp85")
A1_21<- c("ann_gm.ts_Amon_GISS-E2-H.rcp26","ann_gm.ts_Amon_GISS-E2-H.rcp45","ann_gm.ts_Amon_GISS-E2-H.rcp60","ann_gm.ts_Amon_GISS-E2-H.rcp85")
A1_22<- c("ann_gm.ts_Amon_GISS-E2-H-CC.rcp45","ann_gm.ts_Amon_GISS-E2-H-CC.rcp85")
A1_23<- c("ann_gm.ts_Amon_GISS-E2-R.rcp26","ann_gm.ts_Amon_GISS-E2-R.rcp45","ann_gm.ts_Amon_GISS-E2-R.rcp60","ann_gm.ts_Amon_GISS-E2-R.rcp85")
A1_24<- c("ann_gm.ts_Amon_HadCM3.rcp45")
A1_25<- c("ann_gm.ts_Amon_HadGEM2-AO.rcp26","ann_gm.ts_Amon_HadGEM2-AO.rcp45","ann_gm.ts_Amon_HadGEM2-AO.rcp60","ann_gm.ts_Amon_HadGEM2-AO.rcp85")
A1_26<- c("ann_gm.ts_Amon_HadGEM2-CC.rcp45","ann_gm.ts_Amon_HadGEM2-CC.rcp85")
A1_27<- c("ann_gm.ts_Amon_HadGEM2-ES.rcp45","ann_gm.ts_Amon_HadGEM2-ES.rcp60","ann_gm.ts_Amon_HadGEM2-ES.rcp85")
A1_28<- c("ann_gm.ts_Amon_inmcm4.rcp45","ann_gm.ts_Amon_inmcm4.rcp85")
A1_29<- c("ann_gm.ts_Amon_IPSL-CM5A-LR.rcp26","ann_gm.ts_Amon_IPSL-CM5A-LR.rcp45","ann_gm.ts_Amon_IPSL-CM5A-LR.rcp60","ann_gm.ts_Amon_IPSL-CM5A-LR.rcp85")
A1_30<- c("ann_gm.ts_Amon_IPSL-CM5A-MR.rcp26","ann_gm.ts_Amon_IPSL-CM5A-MR.rcp45","ann_gm.ts_Amon_IPSL-CM5A-MR.rcp60","ann_gm.ts_Amon_IPSL-CM5A-MR.rcp85")
A1_31<- c("ann_gm.ts_Amon_IPSL-CM5B-LR.rcp45","ann_gm.ts_Amon_IPSL-CM5B-LR.rcp85")
A1_32<- c("ann_gm.ts_Amon_MIROC4h.rcp45")
A1_33<- c("ann_gm.ts_Amon_MIROC5.rcp26","ann_gm.ts_Amon_MIROC5.rcp45","ann_gm.ts_Amon_MIROC5.rcp60","ann_gm.ts_Amon_MIROC5.rcp85")
A1_34<- c("ann_gm.ts_Amon_MIROC-ESM.rcp26","ann_gm.ts_Amon_MIROC-ESM.rcp45","ann_gm.ts_Amon_MIROC-ESM.rcp60","ann_gm.ts_Amon_MIROC-ESM.rcp85")
A1_35<- c("ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp26","ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp45","ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp60","ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp85")
A1_36<- c("ann_gm.ts_Amon_MPI-ESM-LR.rcp26","ann_gm.ts_Amon_MPI-ESM-LR.rcp45","ann_gm.ts_Amon_MPI-ESM-LR.rcp85")
A1_37<- c("ann_gm.ts_Amon_MPI-ESM-MR.rcp26","ann_gm.ts_Amon_MPI-ESM-MR.rcp45","ann_gm.ts_Amon_MPI-ESM-MR.rcp85")
A1_38<- c("ann_gm.ts_Amon_MRI-CGCM3.rcp26","ann_gm.ts_Amon_MRI-CGCM3.rcp45","ann_gm.ts_Amon_MRI-CGCM3.rcp60","ann_gm.ts_Amon_MRI-CGCM3.rcp85")
A1_39<- c("ann_gm.ts_Amon_MRI-ESM1.rcp85")
A1_40<- c("ann_gm.ts_Amon_NorESM1-M.rcp26","ann_gm.ts_Amon_NorESM1-M.rcp45","ann_gm.ts_Amon_NorESM1-M.rcp60","ann_gm.ts_Amon_NorESM1-M.rcp85")
A1_41<- c("ann_gm.ts_Amon_NorESM1-ME.rcp26", "ann_gm.ts_Amon_NorESM1-ME.rcp45","ann_gm.ts_Amon_NorESM1-ME.rcp60","ann_gm.ts_Amon_NorESM1-ME.rcp85")

AA1<- c(A1_1,A1_2,A1_3,A1_4,A1_5,A1_6,A1_7,A1_8,A1_9,A1_10,
	  A1_11,A1_12,A1_13,A1_14,A1_15,A1_16,A1_17,A1_18,A1_19,A1_20,
	  A1_21,A1_22,A1_23,A1_24,A1_25,A1_26,A1_27,A1_28,A1_29,A1_30,
	  A1_31,A1_32,A1_33,A1_34,A1_35,A1_36,A1_37,A1_38,A1_39,A1_40,A1_41)

	##Read any one .nc file and check variables
	##also check ts read below in variable TS using 
	##all(TS.b==TS[,5][!is.na(TS[,5])])

	BB<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[3],"/",A1_3[1],".nc",sep="")
	fid.b<- nc_open(BB,verbose=TRUE,write=FALSE)
	nc_close(fid.b)
				##identify the variables
				netcdf.file <- BB
				nc = ncdf4::nc_open(netcdf.file)
				variables = names(nc[['var']])
				variables
	TS.b<- ncvar_get(fid.b,"ts")

##Find range of each time series, variable A2
A2<- numeric(0)
	for(i in 1:length(A1)){
	   M1<- eval(parse(text=paste("A1_",i,sep=""))) #read files within each folder

	   for(j in 1:length(M1)){
	   AA<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[i],"/",M1[j],".nc",sep="")
         fid.a<- nc_open(AA,verbose=FALSE,write=FALSE)
	   TS1<- ncvar_get(fid.a,"ts")
	   nc_close(fid.a)
	   A2<- append(A2,length(TS1))
		}
	}

range(A2)		#75 and 547


##Read ts in TS2 and check
TS2<- matrix(NA, nrow=547,ncol=1)

for(i in 1:length(A1)){
	   M1<- eval(parse(text=paste("A1_",i,sep=""))) #read files within each folder
for(j in 1:length(M1)){
	   CC<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[i],"/",M1[j],".nc",sep="")
         fid.c<- nc_open(CC,verbose=FALSE,write=FALSE)
	   TS1<- ncvar_get(fid.c,"ts")
	   TS1<- TS1+0.076
	   nc_close(fid.c)
	   TS_temp<- rep(NA,547)
	   TS_temp[1:length(TS1)]<- TS1
	   TS2<- cbind(TS2,TS_temp)
	   }
}
TS<- TS2[,-1]
dim(TS)

X<- c(1850:(1850+546))

X11()
plot(X,TS[,1],type="n", ylim=c(-2,15),ylab="", xlab="year")
	for(i in 1:ncol(TS)){
	lines(X,TS[,i])
	}
ind<- apply(TS,2,function(x)(which(x>=1.5))[1]) #which(TS[,1]>=1.5)
								#X[which(TS[,1]>=1.5)[1]]

X11()
plot(1850:2100,(TS[,1])[1:251],type="n", ylim=c(-1,3),ylab="", xlab="year")
abline(h=1.5, col="gray")
	for(i in 1:ncol(TS)){
	x<- X[ind[i]]
	if(!is.na(x)){lines((1850:x),(TS[,i])[1:ind[i]])}
	#lines((1850:x),(TS[,i])[1:ind[i]])
	}
AA2<- cbind(X[ind], AA1)
AA3<- cbind((X[ind])[!is.na(ind)],AA1[!is.na(ind)])

#write.table(AA3, "C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/sample_years_from_median.txt")

read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/sample_years_from_median.txt")


